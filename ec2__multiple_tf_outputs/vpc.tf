## Create VPC ##
resource "aws_vpc" "terraform-vpc" {
  cidr_block       = "172.31.0.0/28"
  enable_dns_hostnames = true
  tags = {
    Name = "terraform-demo-vpc"
  }
}

# output "aws_vpc_id" {
#   value = aws_vpc.terraform-vpc.id
# }

## Create Subnets ##
resource "aws_subnet" "terraform-subnet_1" {
  vpc_id     = aws_vpc.terraform-vpc.id
  cidr_block = "172.31.0.1/28"
  availability_zone = "us-east-2b"

  tags = {
    Name = "terraform-subnet_1"
  }
}
