# avoid problem with version update
# terraform 0.13
terraform {
  required_providers {
      # In the rare situation of using two providers that
      # have the same type name -- "http" in this example --
      # use a compound local name to distinguish them.
      aws = {
        source  = "hashicorp/aws"
        version = "~> 3.1.0"
      }
      archive = {
        version = "~> 1.3"
      }  
  }
}